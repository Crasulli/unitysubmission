﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeAttack : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void Awake()
    {
        shootableMask = LayerMask.GetMask("Shootable");

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.LayerMask == Shootable)
        {
            playerInRange = true;
        }
    }
