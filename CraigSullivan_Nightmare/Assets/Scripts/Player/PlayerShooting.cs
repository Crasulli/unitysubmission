﻿using System;
using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    public int damagePerShot = 20;
    public float timeBetweenBullets = 0.15f;
    public float range = 100f;
    public GameObject wall;
    public GameObject WallSpawner;
    public static int wallcount = 0;

    public int damagePerGrenade = 40;
    public GameObject Grenade;
    public GameObject GrenadeSpawner;
    public float timeBetweenGrenades = 60f;
    public float timeBetweenWalls = 60f;

    Rigidbody playerRigidbody;
    float timer;
    Ray shootRay = new Ray();
    RaycastHit shootHit;
    int shootableMask;
    ParticleSystem gunParticles;
    LineRenderer gunLine;
    AudioSource gunAudio;
    Light gunLight;
    float effectsDisplayTime = 0.2f;


    void Awake ()
    {
        shootableMask = LayerMask.GetMask ("Shootable");
        gunParticles = GetComponent<ParticleSystem> ();
        gunLine = GetComponent <LineRenderer> ();
        gunAudio = GetComponent<AudioSource> ();
        gunLight = GetComponent<Light> ();
    }


    void Update ()
    {
        timer += Time.deltaTime;

		if(Input.GetButton ("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0)
        {
            Shoot ();
        }
        if (Input.GetButton("Fire3") && timer >= timeBetweenGrenades && Time.timeScale != 0)
        {
            GrenadeToss();
        }
        if (Input.GetButton("Fire2") && timer >= timeBetweenWalls && Time.timeScale != 0)
        {
            SpawnWall();
        }
        if (timer >= timeBetweenBullets * effectsDisplayTime)
        {
            DisableEffects ();
        }
    }


    public void DisableEffects ()
    {
        gunLine.enabled = false;
        gunLight.enabled = false;
    }


    void Shoot()
    {
        timer = 0f;

        gunAudio.Play();

        gunLight.enabled = true;

        gunParticles.Stop();
        gunParticles.Play();

        gunLine.enabled = true;
        gunLine.SetPosition(0, transform.position);

        shootRay.origin = transform.position;
        shootRay.direction = transform.forward;

        if (Physics.Raycast(shootRay, out shootHit, range, shootableMask))
        {
            EnemyHealth enemyHealth = shootHit.collider.GetComponent<EnemyHealth>();
            if (enemyHealth != null)
            {
                enemyHealth.TakeDamage(damagePerShot, shootHit.point);
            }
            gunLine.SetPosition(1, shootHit.point);
        }
        else
        {
            gunLine.SetPosition(1, shootRay.origin + shootRay.direction * range);
        }

    }

    void GrenadeToss()
    {


        gunAudio.Play();

        gunLight.enabled = true;
    
        timer = 0f;
        Instantiate(Grenade, GrenadeSpawner.transform.position, transform.rotation);
    }

    public void SpawnWall()
    {


        if (wallcount <= 2)
        {
            timer = 0f;
            Instantiate(wall, WallSpawner.transform.position, transform.rotation);
            wallcount += 1;
        }
        

        
    }

 
}



    
