﻿using UnityEngine;
using System.Collections;

public class EnemyAttack : MonoBehaviour
{
    public float timeBetweenAttacks = 0.5f;
    public int attackDamage = 10;


    Animator anim;
    GameObject player;
    PlayerHealth playerHealth;
    EnemyHealth enemyHealth;

    GameObject wall;
    WallHealth wallHealth;

    bool playerInRange;
    bool wallInRange;
    float timer;


    void Awake ()
    {
        player = GameObject.FindGameObjectWithTag ("Player");
        playerHealth = player.GetComponent <PlayerHealth> ();

        wall = GameObject.FindGameObjectWithTag("Wall");
        wallHealth = wall.GetComponent<WallHealth>();
        enemyHealth = GetComponent<EnemyHealth>();
        anim = GetComponent <Animator> ();
    }


    void OnTriggerEnter (Collider other)
    {
        if(other.gameObject == player)
        {
            playerInRange = true;
        }
        if (other.gameObject == wall)
        {
            wallInRange = true;
        }
    }



    void OnTriggerExit (Collider other)
    {
        if(other.gameObject == player)
        {
            playerInRange = false;
        }

        if (other.gameObject == wall)
        {
            wallInRange = false;
        }
    }


    void Update ()
    {
        timer += Time.deltaTime;

        if(timer >= timeBetweenAttacks && playerInRange && enemyHealth.currentHealth > 0)
        {
            Attack ();
        }

        if (timer >= timeBetweenAttacks && wallInRange && enemyHealth.currentHealth > 0)
        {
            AttackWall();
        }
        if (playerHealth.currentHealth <= 0)
        {
            anim.SetTrigger ("PlayerDead");
        }

    }


    void Attack ()
    {
        timer = 0f;

        if(playerHealth.currentHealth > 0)
        {
            playerHealth.TakeDamage (attackDamage);
        }
    }

    public void AttackWall()
    {
        timer = 0f;
        if(wallHealth.currentWallHealth > 0)
        {
            wallHealth.TakeWallDamage(attackDamage);
        }
    }
}
