﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallHealth : MonoBehaviour {

    // Use this for initialization
    public int startingWallHealth = 100;
    public int currentWallHealth;

    GameObject wall;
    




    AudioSource wallAudio;



    void Awake()
    {
        wallAudio = GetComponent<AudioSource>();
    
        currentWallHealth = startingWallHealth;
    }

    public void TakeWallDamage(int amount)
    {

        currentWallHealth -= amount;

        wallAudio.Play();

        if (currentWallHealth <= 0)
        {
            wallAudio.Play();
            Destroy(gameObject, 1f);
            PlayerShooting.wallcount -= 1;
        }
    }
}
